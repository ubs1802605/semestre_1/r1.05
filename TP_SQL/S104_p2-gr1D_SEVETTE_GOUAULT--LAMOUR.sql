/*
Filiere(
    nomFiliere(1), 
    estSelective(NN)
)

Mention(
    [intituleMention (NN), uneFiliere=@Filiere.nomFiliere(NN)](1)
)


Etablissement(
    numEtablissement(1), 
    estPrive
)

Eleve(
    numeroINE(1), 
    nom(2), 
    prenom(2), 
    age, 
    estBoursier, 
    voie
)

Voeu(
    numeroVoeu(1), 
    resultat(NN), 
    dateAppel, 
    dateAcceptation, 
    unEleve=@Eleve.numeroINE(NN), 
    unEtablissement=@Etablissement.numEtablissement(NN),
    uneMention=@Mention.intituleMention(NN)    ATTENTION intituleMention n'est pas clé primaire donc jsp 
)

EtablissementPropose(
    [uneMention=@Mention.intituleMention,
    unEtab=@Etablissement.numEtablissement](1)
)



CONTRAINTES TEXTUELLES :
§ La clé primaire de Mention est la concaténation de la clé de Filière et de l’attribut intituleMention
§ Le résultat d'un vœu, i.e. DOM(resultat), est soit : Non Classé, Classé Non Appelé, Classé Appelé
§ La voie d'un élève , i.e. DOM(voie), est soit : Générale, Technologique, Professionnelle
*/