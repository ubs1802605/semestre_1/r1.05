/*
    EXERCICE 1
*/

/*
    QUESTION 1

    On créer les tables dans l'ordre donné car il respecte les dépendances et on détruit les tables dans l'ordre inverse 
*/

/*
    Enseignant(nomEns(1), prenomEns, adresse, statut)
    Cycle(num(1), enseignantResponsable = @Enseignant.nomEns(UQ)(NN) )
    Cours(nomCours(1), volumeH, lEnseignant=@Enseignant.nomEns(NN), leCycle=@Cycle.num(NN) )
    Requiert([cours=@Cours.nomCours, coursRequis=@Cours.nomCours](1) )
*/

/*
    QUESTION 3
*/
DROP TABLE Requiert;
DROP TABLE Cours;
DROP TABLE Cycle;
DROP TABLE Enseignant;


CREATE TABLE Enseignant
    (
    nomEns VARCHAR2 (20) 
        CONSTRAINT pk_Enseignant PRIMARY KEY,

    prenomEns VARCHAR2 (20),

    adresse VARCHAR2 (20),

    statut VARCHAR2 (20)
    )
;

CREATE TABLE Cycle 
    (
    num NUMBER 
        CONSTRAINT pk_Cycle PRIMARY KEY,

    enseignantResponsable VARCHAR2 (20) 
        CONSTRAINT fk_Cycle_Enseignant REFERENCES Enseignant(nomEns)
        CONSTRAINT uq_enseignantResponsable UNIQUE
        CONSTRAINT nn_enseignantResponsable NOT NULL
    )
;

CREATE TABLE Cours 
    (
    nomCours VARCHAR2(20) 
        CONSTRAINT pk_nomCours PRIMARY KEY,

    volumeH NUMBER,

    lEnseignant VARCHAR2(20) 
        CONSTRAINT fk_Cours_Enseignant REFERENCES Enseignant(nomEns)
        CONSTRAINT nn_lEnseignant NOT NULL,

    leCycle NUMBER
        CONSTRAINT fk_Cours_Cycle REFERENCES Cycle(num)
        CONSTRAINT nn_leCycle NOT NULL
    )
;

CREATE TABLE Requiert
    (
    cours VARCHAR2(20)
        CONSTRAINT fk_Requiert_Cours REFERENCES Cours(nomCours),

    coursRequis VARCHAR2(20)
        CONSTRAINT fk_Requiert_CoursRequis REFERENCES Cours(nomCours),

    CONSTRAINT pk_Requiert PRIMARY KEY(cours, coursRequis)
    )
;



/*
    EXERCICE 2
*/

/*
    Proprietaire(idProprietaire(1), nomProprietaire(NN), prenomProprietaire(NN), emailProprietaire(UQ)(NN))
    Emplacement(idEmplacement(1), longueurEmplacement(NN), coutJournalier(NN))
    Bateau(idBateau(1), nomBateau, longueurBateau(NN), leProprietaire = @Proprietaire.idProprietaire(NN), leStationnement = @Emplacement.idEmplacement(UQ) )
    Reservation(idReservation(1), dateDebut(NN), dateFin(NN), leBateau = @Bateau.idBateau(NN), lEmplacement = @Emplacement.idEmplacement(NN))
*/

DROP TABLE Reservation;
DROP TABLE Bateau;
DROP TABLE Emplacement;
DROP TABLE Proprietaire;

CREATE TABLE Proprietaire
    (
    idProprietaire NUMBER
        CONSTRAINT pk_idProprietaire PRIMARY KEY,

    nomProprietaire VARCHAR2(20)
        CONSTRAINT nn_NomProprietaire NOT NULL,

    prenomProprietaire VARCHAR2(20)
        CONSTRAINT nn_prenomProprietaire NOT NULL,

    emailProprietaire VARCHAR2(20)
        CONSTRAINT uq_emailProprietaire UNIQUE
        CONSTRAINT nn_emailProprietaire NOT NULL
        CONSTRAINT ck_emailProprietaire CHECK(emailProprietaire LIKE '%_@%_.%_')
    )
;

CREATE TABLE Emplacement 
    (
    idEmplacement NUMBER
        CONSTRAINT pk_idEmplacement PRIMARY KEY,

    longueurEmplacement NUMBER
        CONSTRAINT nn_longueurEmplacement NOT NULL,

    coutJournalier VARCHAR2(20)
        CONSTRAINT nn_coutJournalier NOT NULL
    )
;

CREATE TABLE Bateau
    (
    idBateau NUMBER 
        CONSTRAINT pk_idBateau PRIMARY KEY,

    nomBateau VARCHAR2(20),

    longueurBateau NUMBER
        CONSTRAINT nn_longueurBateau NOT NULL
        CONSTRAINT ck_longueurBateauInf20m CHECK(longueurBateau < 20),

    leProprietaire NUMBER
        CONSTRAINT fk_Bateau_Proprietaire REFERENCES Proprietaire(idProprietaire)
        CONSTRAINT nn_leProprietaire NOT NULL,

    leStationnement NUMBER 
        CONSTRAINT fk_Bateau_Emplacement REFERENCES Emplacement(idEmplacement)
        CONSTRAINT uq_leStationnement UNIQUE        
    )
;

CREATE TABLE Reservation
    (
    idReservation NUMBER
        CONSTRAINT pk_idReservation PRIMARY KEY,

    dateDebut DATE
        CONSTRAINT nn_dateDebut NOT NULL,

    dateFin DATE
        CONSTRAINT nn_dateFin NOT NULL,

    leBateau NUMBER
        CONSTRAINT fk_Reservation_Bateau REFERENCES Bateau(idBateau)
        CONSTRAINT nn_leBateau NOT NULL,

    lEmplacement NUMBER
        CONSTRAINT fk_Reservation_Emplacement REFERENCES Emplacement(idEmplacement)
        CONSTRAINT nn_lEmplacement NOT NULL,

    CONSTRAINT ck_dateDebutDateFin CHECK (dateDebut < dateFin)
    )
;


/*
    QUESTION 6
*/
ALTER TABLE Bateau ADD CONSTRAINT uq_nomBateau UNIQUE (nomBateau);

