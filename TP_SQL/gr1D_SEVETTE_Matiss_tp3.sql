-- ======================== Script de création de tables pour le TP3 (cf. TP2, exercice 1)
/* 
Enseignant( nomEns (1), prenomEns, adresse, statut )  
Cycle( num (1), enseignantResponsable = @Enseignant.nomEns (UQ)(NN) )
Cours( nomCours (1), volumeH, lEnseignant = @Enseignant.nomEns (NN), leCycle = @Cycle.num (NN) ) 
Requiert( [ cours = @Cours.nomCours, coursRequis = @Cours.nomCours ](1) )
*/

/* 
QUESTION 3
*/
DELETE FROM Requiert;
DELETE FROM Cours;
DELETE FROM Cycle;
DELETE FROM Enseignant;


DROP TABLE Requiert ;
DROP TABLE Cours ;
DROP TABLE Cycle ; 
DROP TABLE Enseignant ;


CREATE TABLE Enseignant
	(
	nomEns VARCHAR2(20)
		CONSTRAINT pk_Enseignant PRIMARY KEY,	
	prenomEns VARCHAR2(20),
	adresse VARCHAR2(40),
	statut VARCHAR2(20)
	)
;

CREATE TABLE Cycle
	(
	num NUMBER
		CONSTRAINT pk_Cycle PRIMARY KEY,	
	enseignantResponsable VARCHAR2(20) 
		CONSTRAINT fk_Cycle_Enseignant REFERENCES Enseignant(nomEns)
		CONSTRAINT uq_enseignantResponsable UNIQUE
		CONSTRAINT nn_enseignantResponsable NOT NULL
	)
;

CREATE TABLE Cours
	(
	nomCours VARCHAR2(20)
		CONSTRAINT pk_Cours PRIMARY KEY,	
	volumeH NUMBER,
	lEnseignant VARCHAR2(20)
		CONSTRAINT fk_Cours_Enseignant REFERENCES Enseignant(nomEns)
		CONSTRAINT nn_lEnseignant NOT NULL,	
	leCycle NUMBER
		CONSTRAINT fk_Cours_Cycle REFERENCES Cycle(num)	
		CONSTRAINT nn_leCycle NOT NULL
	)
;

CREATE TABLE Requiert
	(
	cours VARCHAR2(20)
		CONSTRAINT fk_Requiert_Cours REFERENCES Cours(nomCours), 
	coursRequis VARCHAR2(20)
		CONSTRAINT fk_Requiert_CoursRequis REFERENCES Cours(nomCours), 
	CONSTRAINT pk_Requiert PRIMARY KEY (cours, coursRequis)
	)
;

/* 
QUESTION 2
*/

INSERT INTO Enseignant (nomEns) Values('RIDARD');
INSERT INTO Enseignant (nomEns) Values('BERG');
INSERT INTO Enseignant (nomEns) Values('TUFFIGOT');
INSERT INTO Enseignant (nomEns) Values('TONIN');
INSERT INTO Enseignant (nomEns) Values('LE MAITRE');

INSERT INTO Cycle (num, enseignantResponsable) Values(1, 'RIDARD');
INSERT INTO Cycle (num, enseignantResponsable) Values(2, 'BERG');
INSERT INTO Cycle (num, enseignantResponsable) Values(3, 'LE MAITRE');

INSERT INTO Cours Values('BDD', 6, 'BERG', 2);
INSERT INTO Cours Values('ECO', 3, 'LE MAITRE', 3);
INSERT INTO Cours Values('MATHS', 7, 'RIDARD', 1);
INSERT INTO Cours Values('COMM', 3, 'TUFFIGOT', 2);
INSERT INTO Cours Values('ANGLAIS', 5, 'TONIN', 1);

INSERT INTO Requiert Values('MATHS', 'ANGLAIS');
INSERT INTO Requiert Values('ECO', 'BDD');

/* 
QUESTION 4
*/
INSERT INTO Enseignant (nomEns) Values (NULL);
INSERT INTO Enseignant (nomEns) Values ('BERG');
INSERT INTO Cycle Values (4, 'BERG');
INSERT INTO Cycle Values (5, 'NULL');


/* 
QUESTION 5
*/
ALTER TABLE Cours ADD CONSTRAINT ck_volumeH CHECK (volumeH > 0);
INSERT INTO Cours Values ('GEOGRAPHIE', 0, 'RIDARD', 3);
ALTER TABLE Cours DROP CONSTRAINT ck_volumeH;


/* 
QUESTION 6
*/
ALTER TABLE Cours ADD age NUMBER;
ALTER TABLE Cours DROP COLUMN age;

ALTER TABLE Cours ADD lEnseignant2 VARCHAR2(20);
ALTER TABLE Cours ADD CONSTRAINT fk_Cours_lEnseignant2 FOREIGN KEY (lEnseignant2) 
REFERENCES Enseignant(nomEns);
ALTER TABLE Cours DROP COLUMN lEnseignant2;


/* 
QUESTION 7
*/
UPDATE Enseignant 
SET statut = NULL
WHERE nomEns = 'RIDARD';


DELETE FROM Requiert
WHERE coursRequis  =  'BDD'
OR coursRequis = 'COMM'
OR cours = 'COMM'
OR cours = 'BDD';

DELETE FROM Cours
WHERE leCycle = 2;

DELETE FROM Cycle
WHERE num = 2;