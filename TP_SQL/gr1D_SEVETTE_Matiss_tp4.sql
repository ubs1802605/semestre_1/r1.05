/*
Exercice 2
*/

/* 
QUESTION 10 et 11 et 12

Concession(idConc(1), nomConc, capital)
Constructeur(idConst(1), nomConst(NN))
Client(idClient(1), nomClient(2), prenomClient(2), emailClient(UQ)(NN))
Voiture(
    immat(1), 
    modele(NN), 
    couleur,
    laConcession=@Concession.idConc,
    leConstructeur=@Constructeur.idConst(NN),
    leClient=@Client.idClient
)
Assurance([UnConstructeur=@Constructeur.idConst, unClient=@Client.idClient](1), dateContrat(NN))
Travail([uneConc=@Concession.idConc, unConst=@Constructeur.idConst](1))


CONTRAINTES TEXTUELLES :
- Voiture[leClient] = Client[idClient]
- Client[idClient] = Assurance[unClient]
- Concession[idConc] = Travail[uneConc]
    Un constructeur travaille avec au moins 2 concessions.
*/

DELETE FROM Voiture ;


DROP TABLE Travail ;
DROP TABLE Assurance ;
DROP TABLE Voiture ;
DROP TABLE Client ; 
DROP TABLE Constructeur ;
DROP TABLE Concession ;


CREATE TABLE Concession
	(
    idConc VARCHAR2(20) 
        CONSTRAINT pk_Concession PRIMARY KEY,
    nomConc VARCHAR2(20),
    capital NUMBER --C'est un double
	)
;

CREATE TABLE Constructeur 
	(
	idConst VARCHAR2(20) 
        CONSTRAINT pk_Constructeur PRIMARY KEY,
    nomConst VARCHAR2(20) 
        CONSTRAINT nn_nomConst NOT NULL
	)
;

CREATE TABLE Client 
	(
    idClient VARCHAR2(20) 
        CONSTRAINT pk_idClient PRIMARY KEY,
    nomClient VARCHAR2(20),
    prenomClient VARCHAR2(20)
	)
;

CREATE TABLE Voiture 
	(
    immat VARCHAR2(20) 
        CONSTRAINT pk_Voiture PRIMARY KEY,
    modele VARCHAR2(20) 
        CONSTRAINT nn_modele NOT NULL,
    couleur VARCHAR2(20),
    laConcession VARCHAR2(20) 
        CONSTRAINT fk_Voiture_Concession REFERENCES Concession(idConc),
    leConstructeur VARCHAR2(20) 
        CONSTRAINT fk_Voiture_Constructeur REFERENCES Constructeur(idConst)
        CONSTRAINT nn_leConstructeur NOT NULL,
    leClient VARCHAR2(20)
        CONSTRAINT fk_Voiture_Client REFERENCES Client(idClient)
	)
;

CREATE TABLE Assurance 
	(
    UnConstructeur VARCHAR2(20)
        CONSTRAINT fk_Assurance_Constructeur REFERENCES Constructeur(idConst),
    unClient VARCHAR2(20)
        CONSTRAINT fk_Assurance_Client REFERENCES Client(idClient),
    dateContrat DATE 
        CONSTRAINT nn_dateContrat NOT NULL,
    CONSTRAINT pk_Assurance PRIMARY KEY (UnConstructeur, unClient)
	)
;

CREATE TABLE Travail 
	(
    uneConc VARCHAR2(20) 
        CONSTRAINT fk_Travail_Concession REFERENCES Concession(idConc),
    unConst VARCHAR2(20) 
        CONSTRAINT fk_Travail_Constructeur REFERENCES Constructeur(idConst),
    CONSTRAINT pk_Travail PRIMARY KEY (uneConc, unConst)
	)
;

/* 
QUESTION 12
*/
ALTER TABLE Client ADD emailClient VARCHAR2(20) 
    CONSTRAINT uq_emailClient UNIQUE
    CONSTRAINT nn_emailClient NOT NULL
    CONSTRAINT ck_emailClient CHECK(emailClient LIKE '%_@%_.%_');

/* 
QUESTION 13
*/
INSERT INTO Constructeur Values ('idConst1', 'FoidjMael');
INSERT INTO Voiture Values ('immat1', 'modele1', NULL, NULL, 'idConst1', NULL);

